create sequence binary_data_SEQ INCREMENT 50;
select setval('binary_data_SEQ', (select max(id) from binary_data));

create sequence drink_SEQ INCREMENT 50;
select setval('drink_SEQ', (select max(id) from drink));

create sequence drink_group_SEQ INCREMENT 50;
select setval('drink_group_SEQ', (select max(id) from drink_group));

create sequence order_drink_SEQ INCREMENT 50;
select setval('order_drink_SEQ', (select max(id) from order_drink));

create sequence role_SEQ INCREMENT 50;
select setval('role_SEQ', (select max(id) from role));

create sequence user__SEQ INCREMENT 50;
select setval('user__SEQ', (select max(id) from user_));

create sequence order__SEQ INCREMENT 50;
select setval('order__SEQ', (select max(id) from order_));

drop sequence if exists hibernate_sequence;