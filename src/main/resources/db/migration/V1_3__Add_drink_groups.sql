CREATE TABLE drink_group (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  order_number int DEFAULT 0,
  PRIMARY KEY (id),
  CONSTRAINT FK_drinkgroup_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_drinkgroup_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE INDEX FK_drinkgroup_creator_IX ON drink_group(creator_id);
CREATE INDEX FK_drinkgroup_modificator_IX ON drink_group(modificator_id);

ALTER TABLE drink
ADD COLUMN group_id int DEFAULT NULL;

ALTER TABLE drink
ADD CONSTRAINT FK_drink_drinkgroup FOREIGN KEY (group_id) REFERENCES drink_group (id);

CREATE INDEX FK_drink_drinkgroup_IX ON drink(group_id);

ALTER TABLE drink
ADD COLUMN order_number int DEFAULT 0;
