ALTER TABLE order_
ADD COLUMN comment varchar(10000);

CREATE TABLE freetextorder (
  id SERIAL,
  content varchar(10000) NOT NULL,
  order_id int NOT NULL,
  PRIMARY KEY(id),
  CONSTRAINT FK_freetextorder_order FOREIGN KEY (order_id) REFERENCES order_(id)
);
