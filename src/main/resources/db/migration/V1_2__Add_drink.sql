INSERT INTO role(name,created,modified,creator_id,modificator_id)
 VALUES ('Printer',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL);

INSERT INTO role_permissions(role_id,permission)
 VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_DRINKS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_DRINKS'),
        ((SELECT id FROM role WHERE name = 'Users'),'SHOW_DRINKS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'CREATE_ORDERS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_ORDERS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_ORDERS'),
        ((SELECT id FROM role WHERE name = 'Users'),'CREATE_ORDERS'),
        ((SELECT id FROM role WHERE name = 'Users'),'SHOW_ORDERS'),
        ((SELECT id FROM role WHERE name = 'Printer'),'PRINT_ORDERS');

CREATE TABLE drink (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  short_description varchar(1023) NOT NULL,
  long_description varchar(65534) NOT NULL,
  picture_id int DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_drink_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_drink_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE INDEX FK_drink_creator_IX ON drink(creator_id);
CREATE INDEX FK_drink_modificator_IX ON drink(modificator_id);

ALTER TABLE drink
  ADD CONSTRAINT FK_drink_binarydata_picture FOREIGN KEY (picture_id) REFERENCES binary_data (id);
CREATE INDEX FK_drink_binarydata_picture_IX ON drink (picture_id);

CREATE TABLE order_ (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  status varchar(50) NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_order_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_order_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE INDEX FK_order_creator_IX ON order_(creator_id);
CREATE INDEX FK_order_modificator_IX ON order_(modificator_id);

CREATE TABLE order_drink (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  drink_id int NOT NULL,
  order_id int NOT NULL,
  amount int NOT NULL DEFAULT 1,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_orderdrink_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_orderdrink_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id),
  CONSTRAINT FK_orderdrink_drink FOREIGN KEY (drink_id) REFERENCES drink (id),
  CONSTRAINT FK_orderdrink_order FOREIGN KEY (order_id) REFERENCES order_ (id)
);

CREATE INDEX FK_orderdrink_creator_IX ON order_drink(creator_id);
CREATE INDEX FK_orderdrink_modificator_IX ON order_drink(modificator_id);
CREATE INDEX FK_orderdrink_drink_IX ON order_drink(drink_id);
CREATE INDEX FK_orderdrink_order_IX ON order_drink(order_id);
