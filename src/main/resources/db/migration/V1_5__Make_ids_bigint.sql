ALTER TABLE binary_data
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE drink
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN picture_id TYPE bigint,
    ALTER COLUMN group_id TYPE bigint;
ALTER TABLE drink_group
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE freetextorder
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN order_id TYPE bigint;
ALTER TABLE order_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE order_drink
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN drink_id TYPE bigint,
    ALTER COLUMN order_id TYPE bigint;
ALTER TABLE role
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role_permissions
    ALTER COLUMN role_id TYPE bigint;
ALTER TABLE user_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN picture_id TYPE bigint;
ALTER TABLE user_role
    ALTER COLUMN user_id TYPE bigint,
    ALTER COLUMN role_id TYPE bigint;