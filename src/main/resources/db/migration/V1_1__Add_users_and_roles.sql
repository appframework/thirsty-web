CREATE TABLE user_ (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  email varchar(255) NOT NULL,
  login varchar(63) NOT NULL,
  name varchar(255) NOT NULL,
  password varchar(40) NOT NULL,
  picture_id int DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE user_
ADD CONSTRAINT FK_user_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE user_
ADD CONSTRAINT FK_user_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_user_creator_IX ON user_(creator_id);
CREATE INDEX FK_user_modificator_IX ON user_(modificator_id);

CREATE TABLE role (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE role
ADD CONSTRAINT FK_role_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE role
ADD CONSTRAINT FK_role_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_role_creator_IX ON role(creator_id);
CREATE INDEX FK_role_modificator_IX ON role(modificator_id);

CREATE TABLE role_permissions (
  role_id int NOT NULL,
  permission varchar(255) DEFAULT NULL
);

ALTER TABLE role_permissions
ADD CONSTRAINT FK_rolepermission_role FOREIGN KEY (role_id) REFERENCES role (id);

CREATE TABLE user_role (
  user_id int NOT NULL,
  role_id int NOT NULL
);

ALTER TABLE user_role
ADD CONSTRAINT FK_userrole_role FOREIGN KEY (role_id) REFERENCES role (id);
ALTER TABLE user_role
ADD CONSTRAINT FK_userrole_user FOREIGN KEY (user_id) REFERENCES user_ (id);

INSERT INTO role(name,created,modified,creator_id,modificator_id)
 VALUES ('Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL),
        ('Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL);

INSERT INTO role_permissions(role_id,permission)
 VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_USERS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_USERS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_ROLES'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_ROLES'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_DRINKS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_DRINKS'),
        ((SELECT id FROM role WHERE name = 'Users'),'SHOW_DRINKS');

INSERT INTO user_(name,login,password,email,created,modified,creator_id,modificator_id)
 VALUES ('Administrator','admin','d033e22ae348aeb5660fc2140aec35850c4da997','admin@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,NULL,NULL);

INSERT INTO user_role(user_id,role_id)
 VALUES ((SELECT id FROM user_ WHERE name = 'Administrator'),(SELECT id FROM role WHERE name = 'Administrators'));

CREATE TABLE binary_data (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  contentType varchar(511) DEFAULT NULL,
  data BYTEA,
  filename varchar(255) DEFAULT NULL,
  size bigint NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY(id),
  CONSTRAINT FK_binarydata_creator FOREIGN KEY (creator_id) REFERENCES user_ (id),
  CONSTRAINT FK_binarydata_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id)
);

CREATE INDEX FK_binarydata_creator_IX ON binary_data(creator_id);
CREATE INDEX FK_binarydata_modificator_IX ON binary_data(modificator_id);

ALTER TABLE user_
  ADD CONSTRAINT FK_user_binarydata_picture FOREIGN KEY (picture_id) REFERENCES binary_data (id);
CREATE INDEX FK_user_binarydata_picture_IX ON user_ (picture_id);
