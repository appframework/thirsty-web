/* eslint-disable */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

/* prettier-ignore */
declare module 'vue' {
  export interface GlobalComponents {
    BAccordion: typeof import('bootstrap-vue-next/components/BAccordion')['BAccordion']
    BAccordionItem: typeof import('bootstrap-vue-next/components/BAccordion')['BAccordionItem']
    BAlert: typeof import('bootstrap-vue-next/components/BAlert')['BAlert']
    BBreadcrumb: typeof import('bootstrap-vue-next/components/BBreadcrumb')['BBreadcrumb']
    BBreadcrumbItem: typeof import('bootstrap-vue-next/components/BBreadcrumb')['BBreadcrumbItem']
    BButton: typeof import('bootstrap-vue-next/components/BButton')['BButton']
    BButtonGroup: typeof import('bootstrap-vue-next/components/BButton')['BButtonGroup']
    BCollapse: typeof import('bootstrap-vue-next/components/BCollapse')['BCollapse']
    BDropdownDivider: typeof import('bootstrap-vue-next/components/BDropdown')['BDropdownDivider']
    BDropdownItem: typeof import('bootstrap-vue-next/components/BDropdown')['BDropdownItem']
    BFormInput: typeof import('bootstrap-vue-next/components/BFormInput')['BFormInput']
    BinaryDataImage: typeof import('./src/appframework/components/BinaryDataImage.vue')['default']
    BInputGroup: typeof import('bootstrap-vue-next/components/BInputGroup')['BInputGroup']
    BModal: typeof import('bootstrap-vue-next/components/BModal')['BModal']
    BNavbar: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbar']
    BNavbarBrand: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbarBrand']
    BNavbarNav: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbarNav']
    BNavbarToggle: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbarToggle']
    BNavForm: typeof import('bootstrap-vue-next/components/BNav')['BNavForm']
    BNavItem: typeof import('bootstrap-vue-next/components/BNav')['BNavItem']
    BNavItemDropdown: typeof import('bootstrap-vue-next/components/BNav')['BNavItemDropdown']
    BPagination: typeof import('bootstrap-vue-next/components/BPagination')['BPagination']
    BreadcrumbElement: typeof import('./src/appframework/components/BreadcrumbElement.vue')['default']
    BSpinner: typeof import('bootstrap-vue-next/components/BSpinner')['BSpinner']
    BTableSimple: typeof import('bootstrap-vue-next/components/BTable')['BTableSimple']
    BTbody: typeof import('bootstrap-vue-next/components/BTable')['BTbody']
    BTd: typeof import('bootstrap-vue-next/components/BTable')['BTd']
    BTfoot: typeof import('bootstrap-vue-next/components/BTable')['BTfoot']
    BTh: typeof import('bootstrap-vue-next/components/BTable')['BTh']
    BThead: typeof import('bootstrap-vue-next/components/BTable')['BThead']
    BTr: typeof import('bootstrap-vue-next/components/BTable')['BTr']
    ColorPickerComponent: typeof import('./src/appframework/components/ColorPickerComponent.vue')['default']
    FileUpload: typeof import('primevue/fileupload')['default']
    IBiBriefcase: typeof import('~icons/bi/briefcase')['default']
    IBiCart: typeof import('~icons/bi/cart')['default']
    IBiCheck: typeof import('~icons/bi/check')['default']
    IBiCopy: typeof import('~icons/bi/copy')['default']
    IBiCupStraw: typeof import('~icons/bi/cup-straw')['default']
    IBiDashCircle: typeof import('~icons/bi/dash-circle')['default']
    IBiDice5Fill: typeof import('~icons/bi/dice5-fill')['default']
    IBiFloppy: typeof import('~icons/bi/floppy')['default']
    IBiGearFill: typeof import('~icons/bi/gear-fill')['default']
    IBiKey: typeof import('~icons/bi/key')['default']
    IBiLock: typeof import('~icons/bi/lock')['default']
    IBiPersonFill: typeof import('~icons/bi/person-fill')['default']
    IBiPersonVcard: typeof import('~icons/bi/person-vcard')['default']
    IBiPlusCircle: typeof import('~icons/bi/plus-circle')['default']
    IBiPower: typeof import('~icons/bi/power')['default']
    IBiPuzzle: typeof import('~icons/bi/puzzle')['default']
    IBiTrash: typeof import('~icons/bi/trash')['default']
    IBiXCircleFill: typeof import('~icons/bi/x-circle-fill')['default']
    IOpenmojiFlagGermany: typeof import('~icons/openmoji/flag-germany')['default']
    IOpenmojiFlagUnitedStates: typeof import('~icons/openmoji/flag-united-states')['default']
    LabelGroupComponent: typeof import('./src/appframework/components/LabelGroupComponent.vue')['default']
    LinkifyText: typeof import('./src/appframework/components/LinkifyText.vue')['default']
    Listbox: typeof import('primevue/listbox')['default']
    ListComponent: typeof import('./src/appframework/components/ListComponent.vue')['default']
    LoadingComponent: typeof import('./src/appframework/components/LoadingComponent.vue')['default']
    MergeDialog: typeof import('./src/appframework/components/MergeDialog.vue')['default']
    Password: typeof import('primevue/password')['default']
    PermissionLinkComponent: typeof import('./src/appframework/components/PermissionLinkComponent.vue')['default']
    ProgressComponent: typeof import('./src/appframework/components/ProgressComponent.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    Select: typeof import('primevue/select')['default']
    SingleComponent: typeof import('./src/appframework/components/SingleComponent.vue')['default']
    SortableColumnHeader: typeof import('./src/appframework/components/SortableColumnHeader.vue')['default']
    Toast: typeof import('primevue/toast')['default']
  }
  export interface ComponentCustomProperties {
    vBColorMode: typeof import('bootstrap-vue-next/directives/BColorMode')['vBColorMode']
  }
}
