import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css'
import './assets/main.css'

import { createApp } from 'vue'
import { createBootstrap } from 'bootstrap-vue-next'
import PrimeVue from 'primevue/config'
import Aura from '@primevue/themes/aura'
import ToastService from 'primevue/toastservice'

import App from './App.vue'
import router from './router'
import i18n, { getDatepickerFormat, getDfnsLocale } from '@/appframework/lib/i18n'

const app = createApp(App)

app.use(router)
app.use(createBootstrap())
app.use(i18n)
app.use(ToastService)
app.use(PrimeVue, {
  locale: {
    firstDayOfWeek: getDfnsLocale().options?.weekStartsOn,
    dateFormat: getDatepickerFormat()
  },
  theme: {
    preset: Aura,
    options: {
      prefix: 'p',
      darkModeSelector: 'system',
      cssLayer: true
    }
  },
  pt: {
    button: {
      root: {
        class: 'btn btn-secondary'
      }
    }
  }
})
app.mount('#app')

//import 'bootstrap/dist/js/bootstrap.js'
