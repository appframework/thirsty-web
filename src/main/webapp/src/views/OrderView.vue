<template>
  <SingleComponent type="order" :stub="stub" name-column="id">
    <template #icon> <IBiCart /> </template>
    <template #form-extra="{ post }">
      <LabelGroup label="order_details">
        <div v-if="!!post.orderDrinks">
          <span v-for="orderDrink in post.orderDrinks" :key="orderDrink.id" class="orderDrinkEntry">
            {{ orderDrink.amount }}x
            {{ drink_lookup[orderDrink.drinkId]?.name || orderDrink.drinkId }}
          </span>
        </div>
        <div
          v-for="(freetextOrder, index) in post.freetextOrders"
          :key="index"
          class="orderDrinkEntry"
        >
          {{ freetextOrder.content }}
        </div>
        <div>{{ post.comment }}</div>
      </LabelGroup>
      <LabelGroup label="ordered_by">
        {{ post.createdBy }}
      </LabelGroup>
      <LabelGroup label="order_status">
        <BButtonGroup style="width: 100%">
          <BButton
            variant="outline-secondary"
            style="width: 33%"
            :disabled="post.status == 'REQUESTED'"
            @click="post.status = 'REQUESTED'"
          >
            REQUESTED
          </BButton>
          <BButton
            variant="outline-secondary"
            style="width: 34%"
            :disabled="post.status == 'PRINTING'"
            @click="post.status = 'PRINTING'"
          >
            PRINTING
          </BButton>
          <BButton
            variant="outline-secondary"
            style="width: 33%"
            :disabled="post.status == 'PRINTED'"
            @click="post.status = 'PRINTED'"
          >
            PRINTED
          </BButton>
        </BButtonGroup>
      </LabelGroup>
    </template>
  </SingleComponent>
</template>

<script setup lang="ts">
import { ref, type Ref } from 'vue'

import LabelGroup from '@/appframework/components/LabelGroupComponent.vue'
import type { Drink, Order } from '@/appframework'
import { getExisting } from '@/appframework/lib/rest'

const drink_lookup: Ref<Record<number, Drink>> = ref({})

function stub() {
  return {} as Order
}

async function init() {
  const drinks = (await getExisting('drink')) as Drink[]
  for (const drink of drinks) {
    drink_lookup.value[drink.id] = drink
  }
}
init()
</script>
