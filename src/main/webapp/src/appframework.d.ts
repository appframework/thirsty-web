/* tslint:disable */
/* eslint-disable */

export interface AbstractDto {
    id: number;
}

export interface BinaryData extends AbstractDto {
    filename: string;
    size: number;
    contentType: string;
}

export interface Credentials {
    username: string;
    password: string;
}

export interface DeletionAction {
    effect: string;
    action: string;
    affected: number;
}

export interface ImportResult {
    skipped: number;
    created: number;
    updated: number;
    deleted: number;
}

export interface Problem {
    title: string;
    status: number;
    detail: string;
}

export interface Progress<T> {
    key: string;
    result: T;
    value: number;
    max: number;
    message: string;
    completed: boolean;
    success: boolean;
}

export interface Role extends AbstractDto {
    name: string;
    permissions: string[];
}

export interface User extends AbstractDto {
    name: string;
    login: string;
    password: string;
    email: string;
    /**
     * @deprecated
     */
    roleIds: number[];
    roles: string[];
    pictureId: number;
}

export interface VersionInformation {
    maven: string;
    git: string;
    buildTimestamp: Date;
}

export interface Drink extends AbstractDto {
    name: string;
    shortDescription: string;
    longDescription: string;
    pictureId: number;
    groupId: number;
    orderNumber: number;
}

export interface DrinkGroup extends AbstractDto {
    name: string;
    orderNumber: number;
}

export interface FreetextOrder {
    content: string;
}

export interface Order extends AbstractDto {
    orderDrinks: OrderDrink[];
    status: Status;
    createdBy: string;
    comment: string;
    freetextOrders: FreetextOrder[];
}

export interface OrderDrink extends AbstractDto {
    drinkId: number;
    amount: number;
}

export interface OrderEntry {
    name: string;
    amount: number;
}

export interface OrderStatusUpdate {
    orderId: number;
    status: Status;
}

export interface PrintJob {
    orderedBy: string;
    orderNumber: number;
    entries: OrderEntry[];
    comment: string;
    freetextEntries: string[];
}

export type Status = "REQUESTED" | "PRINTING" | "PRINTED";
