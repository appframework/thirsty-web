import { createRouter, createWebHistory } from 'vue-router'
import { search } from '@/App.vue'
import { isLoggedIn } from '@/store'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/login',
      component: () => import('../appframework/views/LoginView.vue')
    },
    {
      path: '/logout',
      component: () => import('../appframework/views/LogoutView.vue')
    },
    {
      path: '/roles',
      component: () => import('../appframework/views/RolesView.vue')
    },
    {
      path: '/roles/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/roles/clone/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/users',
      component: () => import('../appframework/views/UsersView.vue')
    },
    {
      path: '/users/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/users/clone/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/profile',
      component: () => import('../appframework/views/ProfileView.vue')
    },
    {
      path: '/drinks',
      component: () => import('../views/DrinksView.vue')
    },
    {
      path: '/drinks/:id',
      component: () => import('../views/DrinkView.vue')
    },
    {
      path: '/drinks/clone/:id',
      component: () => import('../views/DrinkView.vue')
    },
    {
      path: '/drinkgroups',
      component: () => import('../views/DrinkGroupsView.vue')
    },
    {
      path: '/drinkgroups/:id',
      component: () => import('../views/DrinkGroupView.vue')
    },
    {
      path: '/drinkgroups/clone/:id',
      component: () => import('../views/DrinkGroupView.vue')
    },
    {
      path: '/orders',
      component: () => import('../views/OrdersView.vue')
    },
    {
      path: '/orders/:id',
      component: () => import('../views/OrderView.vue')
    },
    {
      path: '/orders/clone/:id',
      component: () => import('../views/OrderView.vue')
    }
  ]
})

router.beforeEach((to, from) => {
  // Not logged in, go to login page
  if (!isLoggedIn() && to.path !== '/login') {
    return { path: '/login' }
  }

  // Clear search between modules
  if (to.path === from.path) {
    return
  }
  let currentModule = from.path.split('/')[1]
  if (currentModule === '') {
    currentModule = 'tasks'
  }
  let nextModule = to.path.split('/')[1]
  if (nextModule === '') {
    nextModule = 'tasks'
  }
  if (currentModule !== nextModule) {
    search.value = ''
  }
  return true
})

export default router
