package de.christophbrill.thirsty.ui.async;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.websocket.CloseReason;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.server.ServerEndpoint;

import io.quarkus.scheduler.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.persistence.model.OrderEntity.Status;
import de.christophbrill.thirsty.persistence.selector.OrderSelector;
import de.christophbrill.thirsty.ui.dto.OrderEntry;
import de.christophbrill.thirsty.ui.dto.OrderStatusUpdate;
import de.christophbrill.thirsty.ui.dto.PrintJob;

@ServerEndpoint("/async/printer")
public class PrinterEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(PrinterEndpoint.class);

    private final ObjectMapper mapper = new ObjectMapper();

    public static final Map<String, Session> CACHE = new HashMap<>();

    @Scheduled(every = "1s")
    public void sendStatus() {
        for (Map.Entry<String, Session> entry : CACHE.entrySet()) {
            String id = entry.getKey();
            EntityManager em = CDI.current().select(EntityManager.class).get();
            OrderEntity order = new OrderSelector(em)
                    .withStatus(Status.REQUESTED)
                    .withSortColumn("created", true)
                    .withLimit(1).find();
            if (order != null) {
                try {
                    entry.getValue().getBasicRemote().sendText(mapper.writeValueAsString(createJob(order)));
                    order.status = Status.PRINTING;
                    order.persist();
                } catch (IOException e) {
                    if (e.getCause() == null || e.getCause().getCause() == null || !(e.getCause().getCause() instanceof ClosedChannelException)) {
                        LOG.error(e.getMessage(), e);
                    }
                    CACHE.remove(id);
                }
            }
        }
    }

    @OnOpen
    public void registerProgressListener(Session session) {
        CACHE.put(session.getId(), session);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        String id = session.getId();
        CACHE.remove(id);
    }

    @OnMessage
    public void onMessage(String message) {
        try {
            OrderStatusUpdate orderStatusUpdate = mapper.readValue(message, OrderStatusUpdate.class);
            OrderEntity order = OrderEntity.findById(orderStatusUpdate.orderId);
            order.status = orderStatusUpdate.status;
            order.persist();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static PrintJob createJob(OrderEntity order) {
        PrintJob job = new PrintJob();
        job.orderedBy = order.createdBy.name;
        job.orderNumber = order.id;
        job.entries = order.orderDrinks
                .stream()
                .map(s -> new OrderEntry(s.drink.name, s.amount))
                .collect(Collectors.toList());
        job.comment = order.comment;
        job.freetextEntries = order.freetextOrders
                .stream()
                .map(s -> s.content)
                .collect(Collectors.toList());
        return job;
    }
}
