package de.christophbrill.thirsty.ui.rest;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.selector.UserSelector;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.thirsty.persistence.model.OrderDrinkEntity;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.persistence.model.Permission;
import de.christophbrill.thirsty.persistence.selector.OrderSelector;
import de.christophbrill.thirsty.ui.dto.Order;

import de.christophbrill.thirsty.util.mappers.OrderMapper;
import jakarta.persistence.FlushModeType;
import jakarta.ws.rs.Path;

@Path("order")
public class OrderService extends AbstractResourceService<Order, OrderEntity> {

    @Override
    protected Class<OrderEntity> getEntityClass() {
        return OrderEntity.class;
    }

    @Override
    protected OrderSelector getSelector() {
        OrderSelector orderSelector = new OrderSelector(em);
        if (this.identity.hasRole(Permission.ADMIN_ORDERS.name())) {
            return orderSelector;
        }
        return orderSelector.withCreator(loadUser());
    }

    @Override
    protected OrderMapper getMapper() {
        return OrderMapper.INSTANCE;
    }

    private UserEntity loadUser() {
        return new UserSelector(em)
                .withLogin(identity.getPrincipal().toString())
                .withFlushMode(FlushModeType.COMMIT)
                .find();
    }

    @Override
    protected String getCreatePermission() {
        return Permission.CREATE_ORDERS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_ORDERS.name();
    }

    @Override
    // Handled in getSelector()
    protected String getUpdatePermission() {
        return Permission.SHOW_ORDERS.name();
    }

    @Override
    // Handled in getSelector()
    protected String getDeletePermission() {
        return Permission.SHOW_ORDERS.name();
    }

    @Override
    protected OrderEntity mapUpdate(Order t) {
        OrderEntity entity = super.mapUpdate(t);
        assureOrderIsSet(entity);
        return entity;
    }

    @Override
    protected OrderEntity mapCreate(Order t) {
        OrderEntity entity = super.mapCreate(t);
        assureOrderIsSet(entity);
        return entity;
    }

    private void assureOrderIsSet(OrderEntity entity) {
        for (OrderDrinkEntity orderDrink : entity.orderDrinks) {
            orderDrink.order = entity;
        }
    }

}
