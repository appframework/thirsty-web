package de.christophbrill.thirsty.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import de.christophbrill.thirsty.persistence.model.Permission;
import de.christophbrill.thirsty.persistence.selector.DrinkGroupSelector;
import de.christophbrill.thirsty.ui.dto.DrinkGroup;
import de.christophbrill.thirsty.util.mappers.DrinkGroupMapper;
import jakarta.ws.rs.Path;

@Path("drinkgroup")
public class DrinkGroupService extends AbstractResourceService<DrinkGroup, DrinkGroupEntity> {

    @Override
    protected Class<DrinkGroupEntity> getEntityClass() {
        return DrinkGroupEntity.class;
    }

    @Override
    protected DrinkGroupSelector getSelector() {
        return new DrinkGroupSelector(em);
    }

    @Override
    protected DrinkGroupMapper getMapper() {
        return DrinkGroupMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_DRINKS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_DRINKS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_DRINKS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_DRINKS.name();
    }

}
