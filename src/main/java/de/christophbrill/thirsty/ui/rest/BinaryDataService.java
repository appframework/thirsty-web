package de.christophbrill.thirsty.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractBinaryDataService;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.persistence.selector.DrinkSelector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.Path;

@Path("binary_data")
public class BinaryDataService extends AbstractBinaryDataService {

    @Override
    protected void nullAndPersistUsers(@Nonnull Long id) {
        List<DrinkEntity> drinks = new DrinkSelector(em)
                .withPictureId(id)
                .findAll();

        drinks.forEach(drink -> {
            drink.picture = null;
            em.persist(drink);
        });
    }

}
