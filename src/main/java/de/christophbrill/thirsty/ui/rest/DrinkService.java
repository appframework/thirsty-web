package de.christophbrill.thirsty.ui.rest;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.persistence.model.Permission;
import de.christophbrill.thirsty.persistence.selector.DrinkSelector;
import de.christophbrill.thirsty.ui.dto.Drink;
import de.christophbrill.thirsty.util.mappers.DrinkMapper;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;

@Path("drink")
public class DrinkService extends AbstractResourceService<Drink, DrinkEntity> {

    @Override
    protected Class<DrinkEntity> getEntityClass() {
        return DrinkEntity.class;
    }

    @Override
    protected DrinkSelector getSelector() {
        return new DrinkSelector(em);
    }

    @Override
    protected DrinkMapper getMapper() {
        return DrinkMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_DRINKS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_DRINKS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_DRINKS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_DRINKS.name();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("{id}/upload")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Long uploadFile(@PathParam("id") long taskId,
                           @RestForm String filename,
                           @RestForm("file") FileUpload file) {
        DrinkEntity drink = getSelector()
                .withId(taskId)
                .find();

        if (drink == null) {
            throw new BadArgumentException("No task with given ID");
        }

        BinaryDataEntity binaryData = new BinaryDataEntity();
        try {
            binaryData.filename = filename;
            byte[] bytes = Files.readAllBytes(file.uploadedFile());
            binaryData.size = bytes.length;
            binaryData.data = bytes;
            binaryData.contentType = URLConnection.guessContentTypeFromName(binaryData.filename);
            binaryData.persist();
            drink.picture = binaryData;
            drink.persist();

            return binaryData.id;
        } catch (IOException e) {
            throw new BadArgumentException("Could not read binary data: " + e.getMessage());
        }
    }
}
