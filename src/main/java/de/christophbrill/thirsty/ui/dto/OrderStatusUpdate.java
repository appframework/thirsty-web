package de.christophbrill.thirsty.ui.dto;

import de.christophbrill.thirsty.persistence.model.OrderEntity.Status;

public class OrderStatusUpdate {

    public int orderId;
    public Status status;

}
