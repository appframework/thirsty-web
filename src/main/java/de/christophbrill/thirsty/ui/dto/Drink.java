package de.christophbrill.thirsty.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Drink extends AbstractDto {

    public String name;
    public String shortDescription;
    public String longDescription;
    public Long pictureId;
    public Long groupId;
    public int orderNumber;

}
