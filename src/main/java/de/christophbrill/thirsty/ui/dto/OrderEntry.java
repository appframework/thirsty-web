package de.christophbrill.thirsty.ui.dto;

public class OrderEntry {

    public String name;
    public int amount;

    public OrderEntry(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

}
