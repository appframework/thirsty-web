package de.christophbrill.thirsty.ui.dto;

import java.util.ArrayList;
import java.util.List;

public class PrintJob {

    public String orderedBy;
    public long orderNumber;
    public List<OrderEntry> entries = new ArrayList<>(0);
    public String comment;
    public List<String> freetextEntries = new ArrayList<>(0);

}
