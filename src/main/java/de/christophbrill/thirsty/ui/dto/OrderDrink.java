package de.christophbrill.thirsty.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class OrderDrink extends AbstractDto {

    public Long drinkId;
    public int amount;

}
