package de.christophbrill.thirsty.ui.dto;

import java.util.List;

import de.christophbrill.appframework.ui.dto.AbstractDto;
import de.christophbrill.thirsty.persistence.model.OrderEntity.Status;

public class Order extends AbstractDto {

    public List<OrderDrink> orderDrinks;
    public Status status;
    public String createdBy;
    public String comment;
    public List<FreetextOrder> freetextOrders;

}
