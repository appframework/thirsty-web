package de.christophbrill.thirsty.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class DrinkGroup extends AbstractDto {

    public String name;
    public int orderNumber;

}
