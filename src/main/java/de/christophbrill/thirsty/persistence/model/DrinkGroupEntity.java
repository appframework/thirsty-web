package de.christophbrill.thirsty.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "DrinkGroup")
@Table(name = "drink_group")
public class DrinkGroupEntity extends DbObject {

    private static final long serialVersionUID = -6280527908625720262L;

    public String name;
    @Column(name = "order_number")
    public int orderNumber;

}
