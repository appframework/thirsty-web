package de.christophbrill.thirsty.persistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "OrderDrink")
@Table(name = "order_drink")
public class OrderDrinkEntity extends DbObject {

    private static final long serialVersionUID = -390009678703297272L;

    @ManyToOne
    @JoinColumn(name = "drink_id", nullable = false)
    public DrinkEntity drink;
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    public OrderEntity order;
    public int amount = 1;

}
