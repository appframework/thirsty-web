package de.christophbrill.thirsty.persistence.model;

import jakarta.persistence.Embeddable;

@Embeddable
public class FreetextOrderEntity {

    public String content;

}
