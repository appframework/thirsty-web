package de.christophbrill.thirsty.persistence.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Drink")
@Table(name = "drink")
public class DrinkEntity extends DbObject {

    private static final long serialVersionUID = -5913676471878096419L;

    public String name;
    @Column(name = "short_description", length = 1023)
    public String shortDescription;
    @Column(name = "long_description", length = 65534)
    public String longDescription;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id")
    public BinaryDataEntity picture;
    @Column(name = "picture_id", updatable = false, insertable = false)
    public Long pictureId;
    @OneToMany(mappedBy = "drink", cascade = CascadeType.REMOVE)
    public List<OrderDrinkEntity> orderDrinks = new ArrayList<>(0);
    @ManyToOne
    @JoinColumn(name = "group_id")
    public DrinkGroupEntity group;
    @Column(name = "order_number")
    public int orderNumber;

}
