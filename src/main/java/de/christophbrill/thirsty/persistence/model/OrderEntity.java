package de.christophbrill.thirsty.persistence.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import org.hibernate.annotations.BatchSize;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Order")
@Table(name = "order_")
public class OrderEntity extends DbObject {

    private static final long serialVersionUID = 5900483528101166600L;

    public enum Status {
        REQUESTED, PRINTING, PRINTED
    }

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    public List<OrderDrinkEntity> orderDrinks = new ArrayList<>(0);
    @Enumerated(EnumType.STRING)
    public Status status = Status.REQUESTED;
    @Column(length = 10000)
    public String comment;
    @BatchSize(size = 50)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "freetextorder", joinColumns = @JoinColumn(name = "order_id"))
    public List<FreetextOrderEntity> freetextOrders = new ArrayList<>(0);

}
