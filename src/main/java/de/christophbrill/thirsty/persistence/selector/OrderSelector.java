package de.christophbrill.thirsty.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.persistence.model.DrinkEntity_;
import de.christophbrill.thirsty.persistence.model.OrderDrinkEntity;
import de.christophbrill.thirsty.persistence.model.OrderDrinkEntity_;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.persistence.model.OrderEntity.Status;
import de.christophbrill.thirsty.persistence.model.OrderEntity_;

public class OrderSelector extends AbstractResourceSelector<OrderEntity> {

    private String search;
    private UserEntity creator;
    private Status status;

    public OrderSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<OrderEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            Join<OrderDrinkEntity, DrinkEntity> joinDrink = from.join(OrderEntity_.orderDrinks).join(OrderDrinkEntity_.drink);
            String pattern = '%' + search.toLowerCase() + '%';
            predicates.add(builder.or(
                    builder.like(builder.lower(joinDrink.get(DrinkEntity_.name)), pattern),
                    builder.like(builder.lower(joinDrink.get(DrinkEntity_.shortDescription)), pattern),
                    builder.like(builder.lower(joinDrink.get(DrinkEntity_.longDescription)), pattern)
            ));
        }

        if (creator != null) {
            predicates.add(builder.equal(from.get(OrderEntity_.createdBy), creator));
        }

        if (status != null) {
            predicates.add(builder.equal(from.get(OrderEntity_.status), status));
        }

        return predicates;
    }

    @Override
    public OrderSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<OrderEntity> getEntityClass() {
        return OrderEntity.class;
    }

    public OrderSelector withCreator(UserEntity creator) {
        this.creator = creator;
        return this;
    }

    public OrderSelector withStatus(Status status) {
        this.status = status;
        return this;
    }

}
