package de.christophbrill.thirsty.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.persistence.model.DrinkEntity_;

public class DrinkSelector extends AbstractResourceSelector<DrinkEntity> {

    private String search;
    private Long pictureId;

    public DrinkSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<DrinkEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(DrinkEntity_.name), '%' + search + '%'));
        }

        if (pictureId != null) {
            predicates.add(builder.equal(from.get(DrinkEntity_.pictureId), pictureId));
        }

        return predicates;
    }

    @Override
    public DrinkSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    public DrinkSelector withPictureId(Long pictureId) {
        this.pictureId = pictureId;
        return this;
    }

    @Override
    @Nonnull
    protected Class<DrinkEntity> getEntityClass() {
        return DrinkEntity.class;
    }

}
