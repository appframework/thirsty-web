package de.christophbrill.thirsty.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity_;

public class DrinkGroupSelector extends AbstractResourceSelector<DrinkGroupEntity> {

    private String search;

    public DrinkGroupSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<DrinkGroupEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(DrinkGroupEntity_.name), '%' + search + '%'));
        }

        return predicates;
    }

    @Override
    public DrinkGroupSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<DrinkGroupEntity> getEntityClass() {
        return DrinkGroupEntity.class;
    }

}
