package de.christophbrill.thirsty.persistence.deletion;

import java.util.List;

import jakarta.persistence.EntityManager;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.thirsty.persistence.model.OrderEntity;

public class UserOrderCascadeDeletion implements CascadeDeletion<UserEntity> {

    @Override
    public int affectedByDeletion(EntityManager em, UserEntity user) {
        return em.createQuery("select count(o) from Order o where o.createdBy = :user or o.modifiedBy = :user", Number.class)
                .setParameter("user", user)
                .getSingleResult()
                .intValue();
    }

    @Override
    public int delete(EntityManager em, UserEntity user) {
        List<OrderEntity> orders = em.createQuery("from Order where createdBy = :user or modifiedBy = :user", OrderEntity.class)
                .setParameter("user", user)
                .getResultList();
        for (OrderEntity order : orders) {
            em.remove(order);
        }
        return orders.size();
    }

    @Override
    public DeletionAction getAction() {
        return new DeletionAction("Orders created or modified by user", "will remove the orders");
    }

}
