package de.christophbrill.thirsty.persistence.deletion;

import java.util.List;

import jakarta.persistence.EntityManager;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;

public class UserDrinkCascadeDeletion implements CascadeDeletion<UserEntity> {

    @Override
    public int affectedByDeletion(EntityManager em, UserEntity user) {
        return em.createQuery("select count(d) from Drink d where d.createdBy = :user or d.modifiedBy = :user", Number.class)
                .setParameter("user", user)
                .getSingleResult()
                .intValue();
    }

    @Override
    public int delete(EntityManager em, UserEntity user) {
        List<DrinkEntity> drinks = em.createQuery("from Drink where createdBy = :user or modifiedBy = :user", DrinkEntity.class)
                .setParameter("user", user)
                .getResultList();
        for (DrinkEntity drink : drinks) {
            em.remove(drink);
        }
        return drinks.size();
    }

    @Override
    public DeletionAction getAction() {
        return new DeletionAction("Drinks created or modified by user", "will remove the drinks");
    }

}
