/*
 * Copyright 2013  Christoph Brill <egore911@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.thirsty.util.listener;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.util.CascadeDeletionRegistry;
import de.christophbrill.appframework.util.listener.AbstractStartupService;
import de.christophbrill.thirsty.persistence.deletion.UserDrinkCascadeDeletion;
import de.christophbrill.thirsty.persistence.deletion.UserOrderCascadeDeletion;
import de.christophbrill.thirsty.persistence.model.Permission;

/**
 * Listener executed during startup, responsible for setting up the
 * java.util.Logging->SLF4J bridge and updating the database.
 *
 * @author Christoph Brill &lt;egore911@gmail.com&gt;
 */
public class StartupListener extends AbstractStartupService {

    @Override
    protected Enum<?>[] getPermissions() {
        return Permission.values();
    }

    @Override
    protected void initCascadeDeletions() {
        CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserOrderCascadeDeletion());
        CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserDrinkCascadeDeletion());
    }

}
