package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import de.christophbrill.thirsty.ui.dto.DrinkGroup;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = DrinkGroupFactory.class)
public interface DrinkGroupMapper extends ResourceMapper<DrinkGroup, DrinkGroupEntity> {

    DrinkGroupMapper INSTANCE = Mappers.getMapper(DrinkGroupMapper.class);

    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Override
    DrinkGroupEntity mapDtoToEntity(DrinkGroup label);

}