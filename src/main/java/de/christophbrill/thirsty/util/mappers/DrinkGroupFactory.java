package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import de.christophbrill.thirsty.ui.dto.DrinkGroup;
import org.mapstruct.ObjectFactory;

public class DrinkGroupFactory {

    @ObjectFactory
    public DrinkGroupEntity createEntity(DrinkGroup dto) {
        if (dto != null && dto.id != null) {
            DrinkGroupEntity entity = DrinkGroupEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("DrinkGroup with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new DrinkGroupEntity();
    }
    
}
