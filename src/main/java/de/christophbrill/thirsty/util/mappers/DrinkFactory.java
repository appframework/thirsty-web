package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.ui.dto.Drink;
import org.mapstruct.ObjectFactory;

public class DrinkFactory {

    @ObjectFactory
    public DrinkEntity createEntity(Drink dto) {
        if (dto != null && dto.id != null) {
            DrinkEntity entity = DrinkEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Drink with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new DrinkEntity();
    }
    
}
