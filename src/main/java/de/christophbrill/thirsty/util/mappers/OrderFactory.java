package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.ui.dto.Order;
import org.mapstruct.ObjectFactory;

public class OrderFactory {

    @ObjectFactory
    public OrderEntity createEntity(Order dto) {
        if (dto != null && dto.id != null) {
            OrderEntity entity = OrderEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Order with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new OrderEntity();
    }
    
}
