package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.thirsty.persistence.model.OrderDrinkEntity;
import de.christophbrill.thirsty.ui.dto.OrderDrink;
import org.mapstruct.ObjectFactory;

public class OrderDrinkFactory {

    @ObjectFactory
    public OrderDrinkEntity createEntity(OrderDrink dto) {
        if (dto != null && dto.id != null) {
            OrderDrinkEntity entity = OrderDrinkEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("OrderDrink with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new OrderDrinkEntity();
    }
    
}
