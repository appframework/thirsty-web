package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.thirsty.persistence.model.OrderDrinkEntity;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.ui.dto.Order;
import de.christophbrill.thirsty.ui.dto.OrderDrink;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = OrderFactory.class)
public interface OrderMapper extends ResourceMapper<Order, OrderEntity> {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "orderDrinks", ignore = true)
    @Override
    OrderEntity mapDtoToEntity(Order dto);

    @AfterMapping
    default void afterDtoToEntity(Order dto, @MappingTarget OrderEntity entity) {
        // Workaround: The default will generate a OrderDrinkEntity to OrderDrinkEntity mapper, prevent this manually mapping ordered drinks
        if (dto.orderDrinks != null) {
            entity.orderDrinks = OrderDrinkMapper.INSTANCE.mapDtosToEntities(dto.orderDrinks);
        } else {
            entity.orderDrinks = null;
        }
    }

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "orderDrinks", ignore = true)
    @Override
    Order mapEntityToDto(OrderEntity entity);

    @AfterMapping
    default void afterEntityToDto(OrderEntity entity, @MappingTarget Order dto) {
        if (entity.createdBy != null) {
            dto.createdBy = entity.createdBy.name;
        } else {
            dto.createdBy = (null);
        }
        if (entity.orderDrinks != null) {
            dto.orderDrinks = OrderDrinkMapper.INSTANCE.mapEntitiesToDtos(entity.orderDrinks);
        } else {
            dto.orderDrinks = null;
        }
    }
}