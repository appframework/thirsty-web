package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import de.christophbrill.thirsty.ui.dto.Drink;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = DrinkFactory.class)
public interface DrinkMapper extends ResourceMapper<Drink, DrinkEntity> {

    DrinkMapper INSTANCE = Mappers.getMapper(DrinkMapper.class);

    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "group", ignore = true)
    @Mapping(target = "picture", ignore = true)
    @Mapping(target = "orderDrinks", ignore = true)
    @Override
    DrinkEntity mapDtoToEntity(Drink label);

    @AfterMapping
    default void afterDtoToEntity(Drink dto, @MappingTarget DrinkEntity entity) {
        if (dto.groupId != null) {
            entity.group = DrinkGroupEntity.findById(dto.groupId);
        } else {
            entity.group = null;
        }
    }

    @Mapping(target = "groupId", ignore = true)
    @Mapping(target = "pictureId", ignore = true)
    @Override
    Drink mapEntityToDto(DrinkEntity entity);

    @AfterMapping
    default void afterEntityToDto(DrinkEntity entity, @MappingTarget Drink dto) {
        if (entity.picture != null) {
            dto.pictureId = entity.picture.id;
        } else {
            dto.pictureId = null;
        }
        if (entity.group != null) {
            dto.groupId = entity.group.id;
        } else {
            dto.groupId = null;
        }
    }
}