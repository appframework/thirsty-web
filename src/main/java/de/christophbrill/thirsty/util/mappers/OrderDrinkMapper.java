package de.christophbrill.thirsty.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import de.christophbrill.thirsty.persistence.model.OrderDrinkEntity;
import de.christophbrill.thirsty.ui.dto.OrderDrink;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = OrderDrinkFactory.class)
public interface OrderDrinkMapper extends ResourceMapper<OrderDrink, OrderDrinkEntity> {

    OrderDrinkMapper INSTANCE = Mappers.getMapper(OrderDrinkMapper.class);

    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "order", ignore = true)
    @Mapping(target = "drink", ignore = true)
    @Override
    OrderDrinkEntity mapDtoToEntity(OrderDrink label);

    @AfterMapping
    default void afterDtoToEntity(OrderDrink dto, @MappingTarget OrderDrinkEntity entity) {
        if (dto.drinkId != null) {
            entity.drink = DrinkEntity.findById(dto.drinkId);
        } else {
            entity.drink = null;
        }
    }

    @Mapping(target = "drinkId", ignore = true)
    @Override
    OrderDrink mapEntityToDto(OrderDrinkEntity entity);

    @AfterMapping
    default void afterEntityToDto(OrderDrinkEntity entity, @MappingTarget OrderDrink dto) {
        if (entity.drink != null) {
            dto.drinkId = entity.drink.id;
        } else {
            dto.drinkId = null;
        }
    }
}