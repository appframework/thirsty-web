package de.christophbrill.thirsty.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class DrinkDaoTest extends AbstractDaoCRUDTest<DrinkEntity> {

    @Override
    protected DrinkEntity createFixture() {
        DrinkEntity drink = new DrinkEntity();
        drink.name = UUID.randomUUID().toString();
        drink.shortDescription = UUID.randomUUID().toString();
        drink.longDescription = UUID.randomUUID().toString();
        return drink;
    }

    @Override
    protected void modifyFixture(DrinkEntity drink) {
        drink.name = UUID.randomUUID().toString();
    }

    @Override
    protected DrinkEntity findById(Long id) {
        return DrinkEntity.findById(id);
    }

    @Override
    protected Long count() {
        return DrinkEntity.count();
    }

}

