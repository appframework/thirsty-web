package de.christophbrill.thirsty.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import io.quarkus.test.junit.QuarkusTest;

import jakarta.inject.Inject;
import java.util.UUID;

@QuarkusTest
public class DrinkGroupDaoTest extends AbstractDaoCRUDTest<DrinkGroupEntity> {

    @Override
    protected DrinkGroupEntity createFixture() {
        DrinkGroupEntity drinkgroup = new DrinkGroupEntity();
        drinkgroup.name = UUID.randomUUID().toString();
        return drinkgroup;
    }

    @Override
    protected void modifyFixture(DrinkGroupEntity drinkgroup) {
        drinkgroup.name = UUID.randomUUID().toString();
    }

    @Override
    protected DrinkGroupEntity findById(Long id) {
        return DrinkGroupEntity.findById(id);
    }

    @Override
    protected Long count() {
        return DrinkGroupEntity.count();
    }

}
