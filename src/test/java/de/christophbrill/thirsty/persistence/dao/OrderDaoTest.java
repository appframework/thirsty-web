package de.christophbrill.thirsty.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.persistence.model.OrderEntity.Status;
import io.quarkus.test.junit.QuarkusTest;

import jakarta.inject.Inject;

@QuarkusTest
public class OrderDaoTest extends AbstractDaoCRUDTest<OrderEntity> {

    @Override
    protected OrderEntity createFixture() {
        return new OrderEntity();
    }

    @Override
    protected void modifyFixture(OrderEntity order) {
        order.status = Status.PRINTED;
    }

    @Override
    protected OrderEntity findById(Long id) {
        return OrderEntity.findById(id);
    }

    @Override
    protected Long count() {
        return OrderEntity.count();
    }

}
