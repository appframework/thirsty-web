package de.christophbrill.thirsty.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.thirsty.persistence.model.DrinkEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class DrinkSelectorTest extends AbstractResourceSelectorTest<DrinkEntity> {

    @Override
    protected DrinkSelector getSelector() {
        return new DrinkSelector(em);
    }

}
