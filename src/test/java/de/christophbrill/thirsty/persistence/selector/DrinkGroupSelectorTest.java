package de.christophbrill.thirsty.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.thirsty.persistence.model.DrinkGroupEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class DrinkGroupSelectorTest extends AbstractResourceSelectorTest<DrinkGroupEntity> {

    @Override
    protected DrinkGroupSelector getSelector() {
        return new DrinkGroupSelector(em);
    }

}
