package de.christophbrill.thirsty.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class OrderSelectorTest extends AbstractResourceSelectorTest<OrderEntity> {

    @Override
    protected OrderSelector getSelector() {
        return new OrderSelector(em);
    }

}
