package de.christophbrill.thirsty.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.thirsty.persistence.model.OrderEntity;
import de.christophbrill.thirsty.ui.dto.Drink;
import de.christophbrill.thirsty.ui.dto.Order;
import de.christophbrill.thirsty.ui.dto.OrderDrink;
import io.quarkus.test.junit.QuarkusTest;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@QuarkusTest
public class OrderCRUDTest extends AbstractCRUDTest<Order> {

    @Override
    protected Order createFixture() {
        Drink drink = new Drink();
        drink.name = "Drink for OrderCRUDTest";

        OrderDrink orderDrink = new OrderDrink();
        orderDrink.amount = 2;
        orderDrink.drinkId = drink.id;

        Order fixture = new Order();
        fixture.orderDrinks = Collections.singletonList(orderDrink);
        fixture.status = OrderEntity.Status.REQUESTED;
        return fixture;
    }

    @Override
    protected String getPath() {
        return "order";
    }

    @Override
    protected Class<Order> getFixtureClass() {
        return Order.class;
    }

    @Override
    protected void modifyFixture(Order fixture) {
        fixture.status = OrderEntity.Status.PRINTING;
    }

    @Override
    protected void compareDtos(Order fixture, Order created) {
        assertThat(created.status, equalTo(fixture.status));
    }

}
