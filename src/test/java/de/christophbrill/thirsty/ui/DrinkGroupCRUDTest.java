package de.christophbrill.thirsty.ui;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.util.UUID;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.thirsty.ui.dto.DrinkGroup;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class DrinkGroupCRUDTest extends AbstractCRUDTest<DrinkGroup> {

    @Override
    protected DrinkGroup createFixture() {
        DrinkGroup fixture = new DrinkGroup();
        fixture.name = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "drinkgroup";
    }

    @Override
    protected Class<DrinkGroup> getFixtureClass() {
        return DrinkGroup.class;
    }

    @Override
    protected void modifyFixture(DrinkGroup fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(DrinkGroup fixture, DrinkGroup created) {
        assertThat(created.name, equalTo(fixture.name));
    }

}
