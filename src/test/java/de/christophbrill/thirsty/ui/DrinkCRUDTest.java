package de.christophbrill.thirsty.ui;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.util.UUID;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.thirsty.ui.dto.Drink;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class DrinkCRUDTest extends AbstractCRUDTest<Drink> {

    @Override
    protected Drink createFixture() {
        Drink fixture = new Drink();
        fixture.name = UUID.randomUUID().toString();
        fixture.shortDescription = UUID.randomUUID().toString();
        fixture.longDescription = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "drink";
    }

    @Override
    protected Class<Drink> getFixtureClass() {
        return Drink.class;
    }

    @Override
    protected void modifyFixture(Drink fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(Drink fixture, Drink created) {
        assertThat(created.name, equalTo(fixture.name));
    }

}
