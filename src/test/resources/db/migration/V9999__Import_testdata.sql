TRUNCATE TABLE
    binary_data,
    drink,
    drink_group,
    freetextorder,
    order_drink,
    order_,
    role,
    role_permissions,
    user_,
    user_role;

INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (1,'Tom Tester','tom','ab4d8d2a5f480a137067da17100271cd176607a1','tom@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (2,'XXX','xxx','b60d121b438a380c343d5ec3c2037564b82ffef3','xxx@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (3,'YYY','yyy','186154712b2d5f6791d85b9a0987b98fa231779c','yyy@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE user__SEQ RESTART WITH 4;

INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (1,'Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (2,'Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (3,'No Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE role_SEQ RESTART WITH 4;

INSERT INTO role_permissions(role_id,permission) VALUES (1,'SHOW_USERS'), (1,'ADMIN_USERS'), (1,'SHOW_ROLES'), (1,'ADMIN_ROLES'), (1,'SHOW_DRINKS'), (1,'ADMIN_DRINKS'), (1,'CREATE_ORDERS'), (1,'SHOW_ORDERS');
INSERT INTO role_permissions(role_id,permission) VALUES (2,'SHOW_USERS');

INSERT INTO user_role(user_id,role_id) VALUES (1,1);
INSERT INTO user_role(user_id,role_id) VALUES (2,2);
INSERT INTO user_role(user_id,role_id) VALUES (3,3);

INSERT INTO drink_group(id,name,order_number) VALUES (1,'Drink Group 17',17);
INSERT INTO drink_group(id,name,order_number) VALUES (2,'Drink Group 18',18);
INSERT INTO drink_group(id,name,order_number) VALUES (3,'Drink Group 20',20);
ALTER SEQUENCE drink_group_SEQ RESTART WITH 4;

INSERT INTO drink(id,name,short_description,long_description,group_id,order_number) VALUES (1,'Drink','Short description','Long description',1,21);
INSERT INTO drink(id,name,short_description,long_description,group_id,order_number) VALUES (2,'Drink','Short description','Long description',1,22);
INSERT INTO drink(id,name,short_description,long_description,group_id,order_number) VALUES (3,'Drink','Short description','Long description',2,23);
INSERT INTO drink(id,name,short_description,long_description,group_id,order_number) VALUES (4,'Drink','Short description','Long description',3,24);
ALTER SEQUENCE drink_SEQ RESTART WITH 5;

INSERT INTO order_(id,status,comment) VALUES (1,'REQUESTED','Comment 1');
INSERT INTO order_(id,status,comment) VALUES (2,'PRINTING','Comment 2');
INSERT INTO order_(id,status,comment) VALUES (3,'PRINTED','Comment 3');
ALTER SEQUENCE order__SEQ RESTART WITH 4;

INSERT INTO order_drink(id,drink_id,order_id,amount) VALUES (1,1,1,10);
INSERT INTO order_drink(id,drink_id,order_id,amount) VALUES (2,2,1,10);
INSERT INTO order_drink(id,drink_id,order_id,amount) VALUES (3,3,1,10);
INSERT INTO order_drink(id,drink_id,order_id,amount) VALUES (4,4,2,10);
INSERT INTO order_drink(id,drink_id,order_id,amount) VALUES (5,4,3,10);
ALTER SEQUENCE order_drink_SEQ RESTART WITH 6;
