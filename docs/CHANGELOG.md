## [1.0.23](https://gitlab.com/appframework/thirsty-web/compare/1.0.22...1.0.23) (2025-02-26)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.30 ([a7e2edb](https://gitlab.com/appframework/thirsty-web/commit/a7e2edbe301ad7b5df703ee74f2fe66a70aa1af0))

## [1.0.22](https://gitlab.com/appframework/thirsty-web/compare/1.0.21...1.0.22) (2025-02-19)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.26 ([f2e0e7f](https://gitlab.com/appframework/thirsty-web/commit/f2e0e7f4707132358906ae0b61ec8e4e7d2a3cbd))
* **deps:** update dependency bootstrap-vue-next to v0.26.28 ([5ab2973](https://gitlab.com/appframework/thirsty-web/commit/5ab2973b5697efb67307b7cd0cfde1229cc0a1b3))
* **deps:** update dependency primelocale to v1.6.0 ([9ac546c](https://gitlab.com/appframework/thirsty-web/commit/9ac546cb251fc6c82002c5f5a4577ccca9962774))
* **deps:** update dependency vue-i18n to v11.1.1 ([c76ee7e](https://gitlab.com/appframework/thirsty-web/commit/c76ee7e7eb5f2471de107b628a9355260178303a))
* **deps:** update quarkus.platform.version to v3.18.2 ([c6f7bec](https://gitlab.com/appframework/thirsty-web/commit/c6f7bec552c8e78c1b18a8d5ddecdd4757fe7619))
* **deps:** update quarkus.platform.version to v3.18.3 ([4d5097a](https://gitlab.com/appframework/thirsty-web/commit/4d5097a90d219c1f1dfe365da34692f0751266a9))

## [1.0.21](https://gitlab.com/appframework/thirsty-web/compare/1.0.20...1.0.21) (2025-02-04)

## [1.0.20](https://gitlab.com/appframework/thirsty-web/compare/1.0.19...1.0.20) (2025-02-03)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.5.0 ([2fc0f26](https://gitlab.com/appframework/thirsty-web/commit/2fc0f2628f14fb597290d89e9075569646247a96))
* **deps:** update dependency bootstrap-vue-next to v0.26.21 ([4348bdf](https://gitlab.com/appframework/thirsty-web/commit/4348bdfd043cbbd52654a06d859c6dcc2c17eb94))
* **deps:** update dependency bootstrap-vue-next to v0.26.22 ([3e16c96](https://gitlab.com/appframework/thirsty-web/commit/3e16c9600e412850fab483c1f7efb7b0c6b43f87))
* **deps:** update dependency primelocale to v1.5.0 ([b4feffe](https://gitlab.com/appframework/thirsty-web/commit/b4feffee774fe1aa29c4ee2fc28d6a86f1d95bc5))
* **deps:** update dependency vue-i18n to v11.1.0 ([9afb5a1](https://gitlab.com/appframework/thirsty-web/commit/9afb5a18070e772aa1fbe6575ffbac9c1bc5a8e7))
* **deps:** update quarkus.platform.version to v3.18.0 ([6d87bb3](https://gitlab.com/appframework/thirsty-web/commit/6d87bb3abb9eceba8bad25142ee662b5442cd07f))
* **deps:** update quarkus.platform.version to v3.18.1 ([d2963f5](https://gitlab.com/appframework/thirsty-web/commit/d2963f56ab2fd1a75ac83782df1b40355772aafb))

## [1.0.19](https://gitlab.com/appframework/thirsty-web/compare/1.0.18...1.0.19) (2025-01-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.20 ([19b56cc](https://gitlab.com/appframework/thirsty-web/commit/19b56cc02ef7f6974f56f8a237959af21ef8ba30))
* **deps:** update dependency primelocale to v1.4.0 ([01d3fbc](https://gitlab.com/appframework/thirsty-web/commit/01d3fbcd07a07c1088ba42298955b25c21683a50))
* **deps:** update quarkus.platform.version to v3.17.7 ([29df3a0](https://gitlab.com/appframework/thirsty-web/commit/29df3a0b6e8b4c01cd95597968edc866d34c312d))

## [1.0.18](https://gitlab.com/appframework/thirsty-web/compare/1.0.17...1.0.18) (2025-01-15)

## [1.0.17](https://gitlab.com/appframework/thirsty-web/compare/1.0.16...1.0.17) (2025-01-15)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.4.0 ([66aa678](https://gitlab.com/appframework/thirsty-web/commit/66aa6785728e051cbe6dd4d33a2c35356564cce8))
* **deps:** update dependency primelocale to v1.3.0 ([4456c50](https://gitlab.com/appframework/thirsty-web/commit/4456c504b098ae0b0c910c540b43019b124b67a0))
* **deps:** update quarkus.platform.version to v3.17.6 ([742aa0b](https://gitlab.com/appframework/thirsty-web/commit/742aa0bda5468f961fb6610e98c9b961921e43de))

## [1.0.16](https://gitlab.com/appframework/thirsty-web/compare/1.0.15...1.0.16) (2025-01-09)


### Bug Fixes

* **deps:** update dependency vue-i18n to v11 ([949ad4b](https://gitlab.com/appframework/thirsty-web/commit/949ad4b09b4336bac39c8779b692f15ca766f235))

## [1.0.15](https://gitlab.com/appframework/thirsty-web/compare/1.0.14...1.0.15) (2025-01-08)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.3.0 ([e8f5966](https://gitlab.com/appframework/thirsty-web/commit/e8f596672e2a44c6a2953d2380fe29e314768458))
* **deps:** update dependency bootstrap-vue-next to v0.26.19 ([7c19f5f](https://gitlab.com/appframework/thirsty-web/commit/7c19f5f011ab81778b46871602ef32f7e59cd28d))
* **deps:** update dependency primelocale to v1.2.3 ([1ac634d](https://gitlab.com/appframework/thirsty-web/commit/1ac634d4855e93739d05cdea551db233af5684f2))

## [1.0.14](https://gitlab.com/appframework/thirsty-web/compare/1.0.13...1.0.14) (2025-01-01)

## [1.0.13](https://gitlab.com/appframework/thirsty-web/compare/1.0.12...1.0.13) (2024-12-25)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.18 ([ee8e6eb](https://gitlab.com/appframework/thirsty-web/commit/ee8e6ebe771f9240a05a98f90ad19790bc3c66ed))
* **deps:** update quarkus.platform.version to v3.17.5 ([40b5541](https://gitlab.com/appframework/thirsty-web/commit/40b554178b8a308e3e18df6931e80d4a94404b69))

## [1.0.12](https://gitlab.com/appframework/thirsty-web/compare/1.0.11...1.0.12) (2024-12-18)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.15 ([ce4bf13](https://gitlab.com/appframework/thirsty-web/commit/ce4bf1330b3bc384831b2f70d9123f4869053f29))

## [1.0.11](https://gitlab.com/appframework/thirsty-web/compare/1.0.10...1.0.11) (2024-12-13)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.14 ([effec5f](https://gitlab.com/appframework/thirsty-web/commit/effec5f215216c703dec1994b6d8dd2adab71f26))

## [1.0.10](https://gitlab.com/appframework/thirsty-web/compare/1.0.9...1.0.10) (2024-12-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12 ([0fee377](https://gitlab.com/appframework/thirsty-web/commit/0fee37775f82bfd627b63c86b941d7a4ba939f36))

## [1.0.9](https://gitlab.com/appframework/thirsty-web/compare/1.0.8...1.0.9) (2024-12-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.3.0 ([5928fa4](https://gitlab.com/appframework/thirsty-web/commit/5928fa4f60f611e5c744336ab74bbe034fe038f4))
* **deps:** update dependency bootstrap-vue-next to v0.26.11 ([b94c2bc](https://gitlab.com/appframework/thirsty-web/commit/b94c2bc3d4f5e100001bda2358dcd6f343c8634e))

## [1.0.8](https://gitlab.com/appframework/thirsty-web/compare/1.0.7...1.0.8) (2024-12-06)


### Bug Fixes

* **deps:** update dependency vue-router to v4.5.0 ([730fd6a](https://gitlab.com/appframework/thirsty-web/commit/730fd6ad54a2b5198dbe106f5152c2cdff64c72d))

## [1.0.7](https://gitlab.com/appframework/thirsty-web/compare/1.0.6...1.0.7) (2024-12-05)

## [1.0.6](https://gitlab.com/appframework/thirsty-web/compare/1.0.5...1.0.6) (2024-12-05)


### Bug Fixes

* **deps:** update dependency primevue to v4.2.4 ([d3feeaf](https://gitlab.com/appframework/thirsty-web/commit/d3feeafed41d4ffab2717c2c23e224df49513259))
* **deps:** update dependency vue-i18n to v10.0.5 ([b47c653](https://gitlab.com/appframework/thirsty-web/commit/b47c6534ca548787583aea142512ada73cd39084))

## [1.0.5](https://gitlab.com/appframework/thirsty-web/compare/1.0.4...1.0.5) (2024-12-04)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.8 ([02ad6ab](https://gitlab.com/appframework/thirsty-web/commit/02ad6ab20a64794e3b80d767bcdb52b9c9023dec))

## [1.0.4](https://gitlab.com/appframework/thirsty-web/compare/1.0.3...1.0.4) (2024-11-28)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.5 ([467d8b6](https://gitlab.com/appframework/thirsty-web/commit/467d8b665fb001f90cfd3441b18fbd220d7cf3dd))
* **deps:** update dependency primelocale to v1.2.2 ([3555961](https://gitlab.com/appframework/thirsty-web/commit/35559613221da160f69a632570d27055d5932b84))
* **deps:** update dependency primevue to v4.2.3 ([f7586c1](https://gitlab.com/appframework/thirsty-web/commit/f7586c11f47d277abcf425c9bc9ee2aabb38a168))

## [1.0.3](https://gitlab.com/appframework/thirsty-web/compare/1.0.2...1.0.3) (2024-11-21)


### Bug Fixes

* **deps:** update dependency vue to v3.5.13 ([8284b07](https://gitlab.com/appframework/thirsty-web/commit/8284b0773eba89c61013136e7e2fd698c6847a9e))

## [1.0.2](https://gitlab.com/appframework/thirsty-web/compare/1.0.1...1.0.2) (2024-11-20)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.26.0 ([354f2d8](https://gitlab.com/appframework/thirsty-web/commit/354f2d849d165dfde36f0920bcc9874f43c4e842))
* **deps:** update dependency primelocale to v1.2.0 ([a9ad2be](https://gitlab.com/appframework/thirsty-web/commit/a9ad2bedbe0fe20174d821a6572e2b34e31dfaa2))

## [1.0.1](https://gitlab.com/appframework/thirsty-web/compare/1.0.0...1.0.1) (2024-11-15)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.14 ([75f992d](https://gitlab.com/appframework/thirsty-web/commit/75f992d9bbb12aee82381c89fc461555e942d7e3))
* **deps:** update dependency primelocale to v1.1.1 ([f231999](https://gitlab.com/appframework/thirsty-web/commit/f231999d4c311d5be1b618d9137ded12a1a09565))
* **deps:** update dependency primevue to v4.2.2 ([b6fd8cd](https://gitlab.com/appframework/thirsty-web/commit/b6fd8cdddc8e341c5d5d218b6a08a30f7296ea26))

# 1.0.0 (2024-10-24)


### Bug Fixes

* **deps:** Continue upgrade to latest appframework ([cbdb0e2](https://gitlab.com/appframework/thirsty-web/commit/cbdb0e20ea600ca1e5157301d1c08d0af7bf78bf))
* **deps:** update dependency org.webjars.npm:angular-translate to v2.19.1 ([aea1e73](https://gitlab.com/appframework/thirsty-web/commit/aea1e738b9bf9935bc220e4700dd1b56e403d6e5))
* **deps:** update dependency org.webjars.npm:angular-translate-loader-static-files to v2.19.1 ([c2f7f27](https://gitlab.com/appframework/thirsty-web/commit/c2f7f271fe4f1bf1a62f5efe5be7c3ec98b51500))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.11 ([6469984](https://gitlab.com/appframework/thirsty-web/commit/6469984ba197bbe47c9a15f72c68570da63f1916))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.12 ([01036a2](https://gitlab.com/appframework/thirsty-web/commit/01036a250fb7f04163533b67d4a41b58fc5d6a6f))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.13 ([3d0a331](https://gitlab.com/appframework/thirsty-web/commit/3d0a331bc8425a4e3e05be7f59c342d822c85e7c))
* **flyway:** Fix copy&pasto ([d52ed1c](https://gitlab.com/appframework/thirsty-web/commit/d52ed1c1f4cddcad07adf8c8575d2bff19af8476))
* **test:** Adapt truncation to cover all tables ([bf2175f](https://gitlab.com/appframework/thirsty-web/commit/bf2175f2a001da4174af14a2536594ec2ad6945d))
* **test:** Use proper testdata ([95aee23](https://gitlab.com/appframework/thirsty-web/commit/95aee23eefb9390351e7c1275135bc59d3a1aab8))
